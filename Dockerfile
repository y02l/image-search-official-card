FROM registry.gitlab.com/y02l/image-search

# download card images
RUN wget https://file.slimemoss.com/yugioh-card/imgs.zip
RUN unzip imgs.zip -d /app/init_image_db
RUN rm imgs.zip

# trim card illust
COPY ./cut_card_illust.py ./
RUN python ./cut_card_illust.py /app/init_image_db
