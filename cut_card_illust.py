import sys
from glob import glob

import cv2

# 200 x 290
# (24, 53), (176, 204)


d = sys.argv[1]
for f in glob(d + '/**/*.png', recursive=True):
    img = cv2.imread(f)
    h, w, _ = img.shape
    img = img[
        int(53 * h / 290): int(204 * h / 290),
        int(24 * w / 200): int(176 * w / 200)
    ]
    cv2.imwrite(f, img)
